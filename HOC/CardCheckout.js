import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";

export default function CardCheckout({ data }) {
  let { handleFunction } = useContext(GlobalContext);
  let { formatRupiah } = handleFunction;

  return (
    <div className="w-full shadow-md rounded-md p-5">
      <div className="">
        <p className="font-bold text-xs truncate mr-5 lg:mr-5">
          {data.product.product_name} {"  "}
        </p>
        <p className="text-xs"> Banyaknya : {data.quantity} Pcs</p>
      </div>
      <div className="font-bold">Total: {formatRupiah(data.unit_price)}</div>
    </div>
  );
}
