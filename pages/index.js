import axios from "axios";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import Card from "../components/Card";
import Category from "../components/Category";
import { GlobalContext } from "../context/GlobalContext";
import Layout from "../widget/Layout";

export async function getServerSideProps() {
  try {
    let res = await fetch(
      `https://service-example.sanbercloud.com/api/product`
    );
    let Product = await res.json();

    return {
      props: {
        Product,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}

export default function Home({ Product }) {
  let { state } = useContext(GlobalContext);
  let { token, setToken, fetchStatus, setFetchStatus } = state;
  const [dataProduct, setDataProduct] = useState(Product);
  const [limit, setLimit] = useState(5);

  const [displaySpinner, setDisplaySpinner] = useState(false);
  const [randomIndex, setRandomIndex] = useState(null);

  const handleCounterFilter = () => {
    setDisplaySpinner(true);

    setTimeout(() => {
      setLimit(limit + 5);
      setDisplaySpinner(false);
    }, 500);
  };

  useEffect(() => {
    let fetchData = async () => {
      try {
        let result = await axios.get(
          `https://service-example.sanbercloud.com/api/product`
        );
        setDataProduct(result.data);
      } catch (error) {
        return alert(error);
      }
    };

    if (Product !== null) {
      let filter = Product.filter((res) => {
        return res.available === 1;
      });
      let random = Math.floor(Math.random() * filter.length);
      setRandomIndex(random);
    }

    if (fetchStatus) {
      fetchData();
      setFetchStatus(false);
    }
  }, [fetchStatus, setFetchStatus, Product]);
  try {
    return (
      <>
        <Layout home>
          <>
            <div className="my-20">
              <h1 className="font-bold text-3xl mt-3">Produk Rekomendasi</h1>
            </div>
            <div className="flex flex-wrap gap-10 justify-center lg:justify-start items-center">
              {dataProduct.length !== 0 &&
                dataProduct
                  .filter((res, index) => {
                    return (
                      res.available === 1 &&
                      index > randomIndex &&
                      index < randomIndex + 3
                    );
                  })
                  .map((res) => {
                    return <Card key={res.id} data={res} />;
                  })}
            </div>
          </>
          <>
            <div className="my-10 pt-20 px-3">
              <span className="text-blue-600 font-bold">
                <Link href={`#`}>Lihat Semua Produk</Link>{" "}
              </span>
              <h1 className="font-bold text-3xl mt-3">Produk Pilihan</h1>
            </div>
            <div className="mb-20 px-3">
              <Category />
            </div>
            <div className="flex flex-wrap gap-10 justify-center lg:justify-start items-center">
              {dataProduct.length !== 0 &&
                dataProduct
                  .filter((res, index) => {
                    return res.available === 1 && index < limit;
                  })
                  .map((res) => {
                    return <Card key={res.id} data={res} />;
                  })}
            </div>
            {!displaySpinner && (
              <div className="container mt-10 flex justify-center items-center">
                <button
                  onClick={handleCounterFilter}
                  type="button"
                  className="py-2.5 flex px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                >
                  View More
                  <svg
                    className="ml-2 w-6 h-6"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M19 9l-7 7-7-7"
                    ></path>
                  </svg>
                </button>
              </div>
            )}
            {displaySpinner && (
              <div className="container mt-10 flex justify-center items-center">
                <div role="status">
                  <svg
                    aria-hidden="true"
                    className="mr-2 w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="currentColor"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentFill"
                    />
                  </svg>
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            )}
          </>
        </Layout>
      </>
    );
  } catch (error) {
    return <p className="p-5">Error API connection...</p>;
  }
}
