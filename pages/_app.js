import Head from "next/head";
import { GlobalProvider } from "../context/GlobalContext";
import "../styles/globals.css";
import "nprogress/nprogress.css";
import { Router } from "next/router";
import nProgress from "nprogress";

Router.events.on("routeChangeStart", nProgress.start);
Router.events.on("routeChangeError", nProgress.done);
Router.events.on("routeChangeComplete", nProgress.done);

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Tokosiapa - Jual Beli Barang Keren Sejagat raya</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="iphone murah, belajar nextjs, tanpa diskon"
        />
        <meta
          name="keywords"
          content="HTML, CSS, JavaScript, Product, ecommerce"
        />
        <meta name="author" content="apri" />
      </Head>
      <GlobalProvider>
        <Component {...pageProps} />
      </GlobalProvider>
    </>
  );
}

export default MyApp;
