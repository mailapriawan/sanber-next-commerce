import axios from "axios";
import Cookies from "js-cookie";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import Layout from "../../widget/Layout";

export default function DetailData() {
  let router = useRouter();
  let { id } = router.query;
  const [dataProduct, setDataProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);

  let { state } = useContext(GlobalContext);
  let { token, setToken } = state;
  const [displaySpinner, setdisplaySpinner] = useState(false);

  const handleCheckout = () => {
    if (!token) {
      router.push(`/user/checkout`);
    } else {
      setdisplaySpinner(true);
    }
  };

  const handlePlus = () => {
    setQuantity(quantity + 1);
  };
  const handleMin = () => {
    quantity > 0 && setQuantity(quantity - 1);
  };

  useEffect(() => {
    if (id !== undefined) {
      axios
        .get(`https://service-example.sanbercloud.com/api/product/${id}`)
        .then((res) => {
          console.log(res);
          let data = res.data;
          setDataProduct(data);
        })
        .catch((err) => {
          alert(err);
        });
    }
    if (Cookies.get(`token_user`) !== undefined) {
      if (token === null) {
        setToken(JSON.parse(Cookies.get("user")));
      }
    }
  }, [id, token, setToken]);

  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
      split = number_string.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
  }

  return (
    <Layout>
      <div className="container">
        <h1 className="text-2xl font-bold py-10">Detail Product</h1>
      </div>
      {dataProduct !== null && (
        <div className=" mt-10">
          <div className="flex-wrap py-10 sm:flex-none lg:flex w-full overflow-hidden bg-white rounded-lg shadow-lg">
            <div className="relative object-cover w-96 h-64 mb-5">
              <Image
                src={dataProduct.image_url}
                alt="Logo"
                layout="fill"
                objectFit="cover"
                quality={80}
              />
            </div>
            <div className="w-full lg:w-2/3 p-4 grow">
              <h1 className="text-2xl font-bold text-gray-900">
                {dataProduct.product_name}
              </h1>
              <p className="mt-2 text-sm text-gray-600">
                {dataProduct.description}
              </p>
              <div className="flex mt-5 item-center ">
                <p className="text-sm font-bold">Stock : {dataProduct.stock}</p>
              </div>
              <div className="flex justify-between mt-3 item-center flex-col lg:flex-row">
                <h1 className="text-xl font-bold text-gray-700">
                  Rp {formatRupiah(dataProduct.price)}
                </h1>
                <div className="items-center">
                  {token !== null && (
                    <>
                      <p className="py-2 text-sm">Kuantitas :</p>
                      <div className="flex">
                        <button
                          onClick={handleMin}
                          className="h-full px-2 text-black bg-gray-200"
                        >
                          -
                        </button>
                        <input
                          value={quantity}
                          className="inline-block w-full h-full text-center focus:outline-none"
                          placeholder="1"
                        />
                        <button
                          onClick={handlePlus}
                          className="h-full px-2 text-black bg-gray-200"
                        >
                          +
                        </button>
                      </div>
                    </>
                  )}
                  {!displaySpinner && (
                    <>
                      {" "}
                      <button
                        onClick={handleCheckout}
                        className="flex items-center justify-center w-full mt-4 px-3 py-4 text-xs font-bold text-white uppercase bg-gray-800 hover:bg-gray-700 rounded"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth={1.5}
                          stroke="currentColor"
                          className="w-6 h-6 mr-3"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z"
                          />
                        </svg>
                        Add to Card
                      </button>
                    </>
                  )}
                  {displaySpinner && (
                    <>
                      <button className="w-full mt-4 px-3 py-2 text-xs font-bold text-white uppercase bg-gray-400 rounded">
                        <div role="status">
                          <svg
                            className="inline mr-2 w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-gray-600 dark:fill-gray-300"
                            viewBox="0 0 100 101"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                              fill="currentColor"
                            />
                            <path
                              d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                              fill="currentFill"
                            />
                          </svg>
                          <span className="sr-only">Loading...</span>
                        </div>
                      </button>
                    </>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </Layout>
  );
}
