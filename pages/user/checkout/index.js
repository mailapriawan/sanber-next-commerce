import axios from "axios";
import Cookies from "js-cookie";
import React, { useContext, useEffect, useRef, useState } from "react";
import { GlobalContext } from "../../../context/GlobalContext";
import CardCheckout from "../../../HOC/CardCheckout";
import emailjs from "@emailjs/browser";
import Layout from "../../../widget/Layout";
import { useRouter } from "next/router";

export default function Checkout() {
  let { state, handleFunction } = useContext(GlobalContext);
  let {
    token,
    setToken,
    getCheckoutUser,
    setCheckoutUser,
    dataCheckoutUser,
    setDataCheckoutUser,
  } = state;
  let { fetchCheckoutUser, sumTotal } = handleFunction;
  let router = useRouter();

  const form = useRef();
  // console.log(form);

  //dataBankState
  const [bank, setBank] = useState(null);
  const [optionBankId, setOptionBankId] = useState(-1);
  const [input, setInput] = useState({
    message: "",
  });

  //indikator
  const [fetchBankStatus, setFetchBankStatus] = useState(true);
  const [fetchCheckoutStatus, setFetchCheckoutStatus] = useState(true);
  const [displaySpinner, setDisplaySpinner] = useState(false);

  const handleOption = (event) => {
    setOptionBankId(event.target.value);
  };

  const handleTransaction = (event) => {
    event.preventDefault();
    if (optionBankId === -1) {
      alert("Pilih Bank Transaksi Kamu !");
    } else {
      axios
        .post(
          `https://service-example.sanbercloud.com/api/transaction/${token.id}`,
          {
            id_bank: optionBankId,
          },
          {
            headers: {
              Authorization: "Bearer" + Cookies.get("token_user"),
            },
          }
        )
        .then((res) => {
          emailjs
            .sendForm(
              "service_gfhyc6o",
              "template_o7rlwga",
              form.current,
              "R4xpPe8u6zQYAzz-B"
            )
            .then(
              (result) => {
                alert(result.text);
              },
              (error) => {
                alert(error.text);
              }
            );
          setDisplaySpinner(false);
          router.push("/");
        })
        .catch((err) => {
          alert(err);
          setDisplaySpinner(false);
        });
    }
  };

  // console.log(dataCheckoutUser);
  useEffect(() => {
    let getArrayCheckout = async () => {
      try {
        let { data } = await axios.get(
          `https://service-example.sanbercloud.com/api/checkout-product-user/${token.id}`,
          {
            headers: {
              Authorization: "Bearer" + Cookies.get("token_user"),
            },
          }
        );
        let result = data
          .filter((res) => {
            return res.is_transaction === 0;
          })
          .map((res) => {
            return res.product.product_name;
          });

        console.log(result);

        if (data.length === 0) {
          setInput({
            ...input,
            message: `Tidak ada checkout !`,
          });
        } else {
          setInput({
            ...input,
            message: `Anda Memiliki Transaksi di tokosiapa.com dengan nama product : \n - ${result.join(
              "\n - "
            )} \n\n Dengan Total Pembayaran : ${sumTotal(
              data
            )} \n Silahkan selesaikan pembayaran dengan bank yang dipilih.`,
          });
        }
      } catch (error) {
        setInput({
          ...input,
          message: `Tidak ada checkout !`,
        });
        alert(error);
      }
    };

    if (Cookies.get(`token_user`) !== undefined) {
      if (token === undefined) {
        setToken(JSON.parse(Cookies.get("user")));
      }
    }
    if (token !== undefined) {
      if (fetchCheckoutStatus) {
        fetchCheckoutUser();
        getArrayCheckout();
        setFetchCheckoutStatus(false);
      }
    }

    //handling error API
    let getbank = async () => {
      try {
        let { data } = await axios.get(
          `https://service-example.sanbercloud.com/api/bank`
        );
        setBank(data);
      } catch (error) {
        alert(error);
      }
    };
    if (fetchBankStatus) {
      getbank();
      setFetchBankStatus(false);
    }
  }, [
    token,
    setToken,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
    getCheckoutUser,
    setCheckoutUser,
    fetchCheckoutUser,
    fetchBankStatus,
    input,
    sumTotal,
  ]);

  return (
    <Layout>
      <div className="mx-auto my-10 p-5">
        <h1 className="text-2xl font-bold">Checkout Product</h1>
        <p className="py-2">produk yang anda pilih : </p>
      </div>
      <div>
        <div className="flex flex-row flex-wrap w-full overflow-hidden bg-white rounded-lg shadow-lg mb-16">
          <div className="flex flex-col w-2/3 p-4 mt-16 grow lg:mt-0">
            {dataCheckoutUser !== null &&
              dataCheckoutUser
                .filter((res) => {
                  return res.is_transaction === 0;
                })
                .map((res) => {
                  return <CardCheckout key={res.id} data={res} />;
                })}
            {dataCheckoutUser !== null && dataCheckoutUser.length === 0 && (
              <div>
                <span class="bg-gray-200 text-gray-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-gray-300">
                  Tidak ada Transaksi
                </span>
              </div>
            )}
            <form onSubmit={handleTransaction} ref={form} className="mt-5">
              <div className="w-full mt-5 p-5 rounded-md bg-white shadow-md">
                <div>
                  <p className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Pilih Bank Pembayaran :{" "}
                  </p>
                  <div className="py-2">
                    <select
                      id="bank"
                      value={optionBankId}
                      onChange={handleOption}
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    >
                      <option defaultValue={-1}>Pilih Bank</option>
                      {bank !== null &&
                        bank.map((res) => {
                          return (
                            <option key={res.id} value={res.id}>
                              {res.bank_name}
                            </option>
                          );
                        })}
                    </select>
                  </div>
                  <div className="">
                    <label
                      htmlFor="helper-text"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Masukan Identitas Anda :
                    </label>
                    {/* input name */}
                    <div>
                      <div>
                        <input
                          name="user_name"
                          type="text"
                          id="helper-text"
                          aria-describedby="helper-text-explanation"
                          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                          placeholder="Nama..."
                        />
                      </div>

                      {/* input email */}
                      <div>
                        <input
                          name="user_email"
                          type="email"
                          id="helper-text"
                          aria-describedby="helper-text-explanation"
                          className="bg-gray-50 border mt-2 border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                          placeholder="Email..."
                        />
                      </div>

                      {/* Text area */}
                      <div className="relative mt-3">
                        <label
                          htmlFor="message"
                          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                        >
                          Pesan
                        </label>
                        <textarea
                          name="message"
                          id="message"
                          rows={4}
                          className="block p-2.5 w-full h-auto text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                          placeholder="Masukan pesan anda..."
                          defaultValue={input.message}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* div button bayar */}
              <div className="flex flex-col justify-between mt-3 items-center lg:flex-row">
                {dataCheckoutUser !== null && (
                  <div className="flex items-center justify-start mt-8 p-5">
                    <div>
                      {" "}
                      <p>
                        Anda melakukan{" "}
                        {dataCheckoutUser !== null && getCheckoutUser} produk
                        checkout
                      </p>
                      <h1 className="text-xl font-bold text-gray-700">
                        Total : Rp{" "}
                        {dataCheckoutUser !== null &&
                          sumTotal(dataCheckoutUser)}
                      </h1>
                    </div>
                  </div>
                )}
                <div className="relative mt-16 lg:mt-0">
                  {!displaySpinner && (
                    <button
                      type="submit"
                      className="w-full mt-4 px-6 py-2 text-xs font-bold text-white uppercase bg-gray-800 hover:bg-gray-700 rounded-lg"
                      value="send"
                    >
                      Bayar
                    </button>
                  )}

                  {displaySpinner && (
                    <button
                      type="submit"
                      className="w-full mt-4 px-6 py-2 text-xs font-bold text-white uppercase bg-gray-800 rounded-lg"
                      value="send"
                    >
                      <div role="status">
                        <svg
                          className="inline ml-3 mr-3 w-4 h-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-600 dark:fill-gray-300"
                          viewBox="0 0 100 101"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                            fill="currentColor"
                          />
                          <path
                            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                            fill="currentFill"
                          />
                        </svg>
                        <span className="sr-only">Loading...</span>
                      </div>
                    </button>
                  )}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  );
}
