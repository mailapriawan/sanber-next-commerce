import axios from "axios";
import Cookies from "js-cookie";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../../../context/GlobalContext";
import Layout from "../../../widget/Layout";

export default function Transaction() {
  let { state, handleFunction } = useContext(GlobalContext);
  let {
    token,
    setToken,
    getCheckoutUser,
    setCheckoutUser,
    dataCheckoutUser,
    setDataCheckoutUser,
  } = state;
  let { fetchCheckoutUser, sumTotal, formatRupiah } = handleFunction;

  //data
  const [data, setData] = useState(null);
  //indikator
  const [fetchTransactionStatus, setFetchTransactionStatus] = useState(true);

  // console.log(data);

  const onClickTransaction = (event) => {
    let idTransaksi = event.target.value;

    axios
      .post(
        `https://service-example.sanbercloud.com/api/transaction-completed/${idTransaksi}/${token.id}`,
        {},
        {
          headers: {
            Authorization: "Bearer" + Cookies.get("token_user"),
          },
        }
      )
      .then((res) => {
        console.log(res);
        setFetchTransactionStatus(true);
      })
      .catch((err) => {
        alert(err);
      });
  };

  const deleteTransaction = (event) => {
    let idTransaksi = event.target.value;

    axios
      .delete(
        `https://service-example.sanbercloud.com/api/transaction/${idTransaksi}`,
        {
          headers: {
            Authorization: "Bearer" + Cookies.get("token_user"),
          },
        }
      )
      .then((res) => {
        console.log(res);
        setFetchTransactionStatus(true);
      })
      .catch((err) => {
        alert(err);
      });
  };

  useEffect(() => {
    if (Cookies.get(`token_user`) !== undefined) {
      if (token === undefined) {
        setToken(JSON.parse(Cookies.get("user")));
      }
    }
    if (token !== undefined) {
      if (fetchTransactionStatus) {
        axios
          .get(
            `https://service-example.sanbercloud.com/api/transaction-user/${token.id}`,
            {
              headers: {
                Authorization: "Bearer" + Cookies.get("token_user"),
              },
            }
          )
          .then((res) => {
            // console.log(res);
            setData(res.data);
            // console.log(data);
          });
        setFetchTransactionStatus(false);
      }
    }
  }, [token, setToken, fetchTransactionStatus, setFetchTransactionStatus]);
  return (
    <Layout>
      <div className="py-10">
        <div className="mx-auto p-5">
          <h1 className="text-2xl font-bold">Transaction </h1>
          <p className="py-2 font-bold text-lg text-gray-400">
            Berikut daftar transaksi anda :{" "}
          </p>
        </div>

        {data !== null &&
          data
            .filter((res) => {
              return res.status === "Transaksi terdaftar";
            })
            .map((res) => {
              return (
                <div
                  key={res.id}
                  className="w-full lg:flex lg:justify-between items-center shadow-md rounded-md p-5 "
                >
                  <div>
                    <div className="">
                      <p className="text-rose-600 font-semibold truncate mr-5 lg:mr-5">
                        {res.status} {"  "}
                      </p>
                      <p className="text-xs py-2">
                        Code Transaksi : {res.transaction_code}
                      </p>
                      <hr />
                    </div>
                    <div className="font-semibold text-sm">
                      - Nama : {res.user.name}{" "}
                    </div>
                    <div className="font-semibold text-sm">
                      - Bank : {res.bank.bank_name} - {res.bank.virtual_account}
                    </div>
                    <div className="font-semibold text-sm">
                      - Total: Rp. {formatRupiah(res.total)}
                    </div>
                  </div>
                  <div className="flex lg:flex-none justify-center lg:justify-end mx-auto lg:mx-0 mt-3">
                    <div>
                      <button
                        onClick={onClickTransaction}
                        value={res.id}
                        type="button"
                        className="text-white bg-gradient-to-br lg:w-56 from-green-400 to-blue-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-green-200 dark:focus:ring-green-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                      >
                        Selesaikan Transaksi
                      </button>
                    </div>
                  </div>
                </div>
              );
            })}

        <p className="p-5 mt-10 font-bold text-lg text-gray-400">
          Transaksi Sudah Selesai :{" "}
        </p>

        {data !== null &&
          data
            .filter((res) => {
              return res.status === "Transaksi selesai";
            })
            .map((res) => {
              return (
                <div
                  key={res.id}
                  className="w-full lg:flex lg:justify-between items-center shadow-md rounded-md p-5 "
                >
                  <div>
                    <div className="">
                      <p className=" text-green-600 font-semibold truncate mr-5 lg:mr-5">
                        {res.status} {"  "}
                      </p>
                      <p className="text-xs py-2">
                        Code Transaksi : {res.transaction_code}
                      </p>
                      <hr />
                    </div>
                    <div className="font-semibold text-sm">
                      - Nama : {res.user.name}{" "}
                    </div>
                    <div className="font-semibold text-sm">
                      - Bank : {res.bank.bank_name} - {res.bank.virtual_account}
                    </div>
                    <div className="font-semibold text-sm">
                      - Total: Rp. {formatRupiah(res.total)}
                    </div>
                  </div>
                  <div className="flex lg:flex-none justify-center lg:justify-end mx-auto lg:mx-0 mt-3">
                    <button
                      onClick={deleteTransaction}
                      value={res.id}
                      type="button"
                      className="text-white bg-gradient-to-br lg:w-56 from-pink-500 to-orange-400 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                    >
                      Hapus Transaksi
                    </button>
                  </div>
                </div>
              );
            })}
      </div>
    </Layout>
  );
}
