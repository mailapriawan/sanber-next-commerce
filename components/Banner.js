import { Carousel } from "flowbite-react";
import Image from "next/image";
import React from "react";

export default function Banner() {
  return (
    <>
      <div className="h-56 sm:h-64 xl:h-96 2xl:h96 container mx-auto">
        <Carousel>
          <div className="relative h-32 lg:h-full">
            <Image
              src="/images/banner1.png"
              alt="banner 1"
              fill
              sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw"
              priority
            />
          </div>
          {/* items 2 */}
          <div className="relative h-32 lg:h-full">
            <Image
              src="/images/banner2.png"
              alt="banner 1"
              fill
              sizes="(max-width: 768px) 100vw,
              (max-width: 1200px) 50vw,
              33vw"
              priority
            />
          </div>
        </Carousel>
      </div>
    </>
  );
}
